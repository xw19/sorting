#include <stdio.h>

void swap(int *arr, int pos1, int pos2) {
	int temp = arr[pos1];
	arr[pos1] = arr[pos2];
	arr[pos2] = temp;
}


int partition(int *arr, int low, int high) {
	int pivot = arr[low];
	int i = low;
	int j = high;
	
	while(i < j) {
		do {
			i++;
		} while(arr[i] <= pivot);
		
		do {
			j--;
		} while(arr[j] > pivot);
		if ( i < j )
			swap(arr, i, j);
		
	}
	swap(arr, low, j);
	return j;
}


void quicksort(int *arr, int low, int high) {
	if (low < high) {
		int j = partition(arr, low, high);
		quicksort(arr, low, j);
		quicksort(arr, j+1, high);
	}
}

void print_list(int *arr, int n) {
	int i;
	for (i =0 ;i < n; i++) {
		printf("%d => %d\n", i, arr[i]);
	}
	printf("-----------------------------------------------\n");
}


int main() {
	int arr[] = {10, 30, 60, 0, 50, 40, 20, 1, 2, 300, 500, 101, 4, 102};
	int n = sizeof(arr)/sizeof(arr[0]);
	print_list(arr, n);
	quicksort(arr, 0, n);
	print_list(arr, n);
	return 0;
}